<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class BookSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books')->truncate();

        for ($x = 1; $x < 21; $x++) {
            DB::table('books')->insert([
                'title'                 => 'title ' . $x,
                'author'                => 'autor ' . $x,
                'year_of_publication'   => '2015',
                'language'              => 'leguage' . $x,
                'language_origin'       => 'language_origin' . $x,
            ]);
        };
    }
}
