'use strict';

/*global app: false */

/**
 * The home controller.
 */
app.controller('HomeCtrl', ['$scope', '$window', 'HomeFactory', '$stateParams',
    function ($scope, $window, HomeFactory, $stateParams) {


        $scope.getAllBooks = function () {
            HomeFactory.getAllBooks().then(function (response) {
                $scope.books = response.data.books;
            })
        };

        $scope.getBook = function () {
            HomeFactory.getBook($stateParams.id).then(function (response) {
                $scope.book = response.data.book;
            })
        };

        $scope.pageChanged = function() {
            $window.scrollTo(0, 0);
        };
    }]);