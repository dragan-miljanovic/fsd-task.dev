'use strict';

/*global app: false */

/**
 * The admin controller.
 */
app.controller('AdminCtrl', ['$rootScope', '$scope', '$alert', '$auth',
    function($rootScope, $scope, $alert, $auth) {

        $scope.isAuthenticated = function () {
            return $auth.isAuthenticated();
        };

        /**
         * Submits the login form.
         */
        $scope.submit = function () {
            $auth.setStorage('sessionStorage');
            $auth.login({username: $scope.email, password: $scope.password})
                .then(function (response) {
                    $alert({
                        content: 'Welcome Admin.',
                        animation: 'fadeZoomFadeDown',
                        type: 'material',
                        duration: 3
                    });
                })
                .catch(function (response) {
                    $alert({
                        content: response.data.message,
                        animation: 'fadeZoomFadeDown',
                        type: 'material',
                        duration: 3
                    });
                });
        };

        /**
         * Authenticate with a social provider.
         *
         * @param provider The name of the provider to authenticate.
         */
        $scope.authenticate = function (provider) {
            $auth.authenticate(provider)
                .then(function () {
                    $alert({
                        content: 'Welcome Admin.',
                        animation: 'fadeZoomFadeDown',
                        type: 'material',
                        duration: 3
                    });
                })
                .catch(function (response) {
                    $alert({
                        content: response.data.message,
                        animation: 'fadeZoomFadeDown',
                        type: 'material',
                        duration: 3
                    });
                });
        };

    }]);
