'use strict';

/*global app: false */

/**
 * The sign out controller.
 */
app.controller('SignOutCtrl', ['$window', '$rootScope', '$auth', '$alert',
    function($window, $rootScope, $auth, $alert) {
    if (!$auth.isAuthenticated()) {
        return;
    }

    $auth.logout()
        .then(function() {
            $alert({
                content: 'Logged out.',
                animation: 'fadeZoomFadeDown',
                type: 'material',
                duration: 3
            });
        });
}]);
