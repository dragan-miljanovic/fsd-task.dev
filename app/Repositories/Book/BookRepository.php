<?php

namespace App\Repositories\Book;

use App\Models\Book\BookModel;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class BookRepository
 * @package App\Repositories\Book
 */
class BookRepository
{
    /**
     * Method for getting all books from db.
     *
     * @return Collection
     */
    public function getAllBooks()
    {
        return BookModel::all();
    }

    /**
     * Method for creating new book in db.
     *
     * @param array $params
     * @return bool
     */
    public function createNewBook($params)
    {
        $book = new BookModel();

        $book->title = $params['title'];
        $book->author = $params['author'];
        $book->year_of_publication = $params['year_of_publication'];
        $book->language = $params['language'];
        $book->language_origin = $params['language_origin'];

        $book->save();
    }

    /**
     * Method for getting book by id from db.
     *
     * @param int $id
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function getBookById($id)
    {
        return BookModel::find($id);
    }

    /**
     * Method for updating book in db.
     *
     * @param array $params
     * @param int $id
     * @return bool
     */
    public function updateBook($params, $id)
    {
        $book = BookModel::find($id);

        $book->title = $params['title'];
        $book->author = $params['author'];
        $book->year_of_publication = $params['year_of_publication'];
        $book->language = $params['language'];
        $book->language_origin = $params['language_origin'];

        $book->save();
    }

    /**
     * Method for getting books from bd by search term.
     *
     * @param $term
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function searchBooks($term)
    {
        return BookModel::where('author', 'LIKE', '%'.$term.'%')
            ->orWhere('year_of_publication', 'LIKE', '%'.$term.'%')
            ->get();
    }

    /**
     * Method for deleting the record from DB.
     *
     * @param $id int
     */
    public function deleteBook($id)
    {
        $book = BookModel::find($id);

        $book->delete();
    }
}