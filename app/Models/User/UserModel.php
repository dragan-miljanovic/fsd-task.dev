<?php

namespace App\Models\User;

use Illuminate\Foundation\Auth\User as Authenticatable;

class UserModel extends Authenticatable
{

    /**
     * Table name.
     *
     * @var string $table
     */
    protected $table = "users";

    /**
     * The attributes that are mass assignable.
     *
     * @var array $fillable
     */
    protected $fillable = [
        'username',
        'password',
        'active',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array $hidden
     */
    protected $hidden = [
        'password',
        'token',
    ];

    /**
     * Rules to be passed while authenticate a user.
     *
     * @var array $getUserRules
     */
    public static $authRules = [
        'username' => 'required|email|exists:users,username',
        'password' => 'required|min:2|max:16'
    ];

    /**
     * Rules to be passed while creating new user.
     *
     * @var array $rules
     */
    public static $rules = [
        'username'              => 'required|email|unique:users,username|max:32',
        'password'              => 'required|min:5|max:60',
        'active'                => 'integer'
    ];
}