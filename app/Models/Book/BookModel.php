<?php

namespace App\Models\Book;

use Illuminate\Database\Eloquent\Model;

/**
 * Class BookModel
 * @package App\Models\Book
 */
class BookModel extends Model
{

    /**
     * Table name.
     *
     * @var string $table
     */
    protected $table = "books";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'author',
        'year_of_publication',
        'language',
        'language_origin',
    ];

    /**
     * Rules to be passed while creating new book.
     *
     * @var array
     */
    public static $rules = [
        'title'                 => 'required|min:4|max:64',
        'author'                => 'required|min:4|max:64',
        'year_of_publication'   => 'required|min:3|max:4',
        'language'              => 'required|min:4|max:32',
        'language_origin'       => 'required|min:4|max:32',
    ];



    /**
     * Rules to be passed while searching a book.
     *
     * @var array
     */
    public static $rulesSearch = [
        'term' => 'required'
    ];
}