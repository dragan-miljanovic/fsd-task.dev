<?php

namespace App\Http\Controllers;

use App\Models\Book\BookModel;
use App\Repositories\Book\BookRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

/**
 * Class BookController
 * @package App\Http\Controllers
 */
class BookController extends Controller
{

    /**
     * @var BookRepository
     */
    protected $bookHandler;


    /**
     * BookController constructor.
     *
     * @param BookRepository $bookRepository
     */
    public function __construct(BookRepository $bookRepository)
    {
        $this->bookHandler = new $bookRepository;

        $this->middleware('jwt.auth')->except(['index', 'show']);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $books = $this->bookHandler->getAllBooks();
        } catch (\Exception $e) {
            Log::error('Error while fetching collection: ', ['message' => $e->getMessage()]);

            return response()->json([ 'message' => trans('response.not_fetched') ], 500);
        }

        if (empty($books))
        {
            return response()->json(['message' => trans('response.nodata')], 404);
        }

        return response()->json([ 'message' => trans('response.success'), 'books' => $books ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $params = $request->all();

        $valid = Validator::make($params, BookModel::$rules);
        if ($valid->fails()) {
            return response()->json([ 'message' => trans('response.invalid') ], 400);
        }

        try {
            $this->bookHandler->createNewBook($params);
        } catch (\Exception $e) {
            Log::error('Error while creating new record: ', ['message' => $e->getMessage()]);

            return response()->json([ 'message' => trans('response.not_created') ], 500);
        }

        return response()->json([ 'message' => trans('response.created') ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $book = $this->getBookById($id);

        if (!$book)
        {
            return response()->json([ 'message' => trans('response.not_found') ], 404);
        }

        return response()->json(['message' => trans('response.success'), 'book' => $book ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $params = $request->all();

        $valid = Validator::make($params, BookModel::$rules);
        if ($valid->fails()) {
            return response()->json(array('message' => trans('response.invalid')), 400);
        }

        $book = $this->getBookById($id);

        if (empty($book))
        {
            return response()->json([ 'message' => trans('response.not_found') ], 404);
        }

        try {
            $this->bookHandler->updateBook($params, $id);
        } catch (\Exception $e) {
            Log::error('Error while updating the record: ', ['message' => $e->getMessage()]);

            return response()->json([ 'message' => trans('response.not_updated') ], 500);
        }

        return response()->json([ 'message' => trans('response.updated') ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $book = $this->getBookById($id);

        if (empty($book))
        {
            return response()->json([ 'message' => trans('response.not_found') ], 404);
        }

        try
        {
            $this->bookHandler->deleteBook($id);
        } catch (\Exception $e) {
            Log::error('Error while deleting the record: ', ['message' => $e->getMessage()]);

            return response()->json(['message' => trans('response.not_deleted')], 500);
        }
        return response()->json(['message' => trans('response.deleted')], 200);
    }

    /**
     * Method for getting books by search term.
     *
     * @param \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function searchBooks(Request $request)
    {
        $params = $request->all();
        $valid = Validator::make($params, BookModel::$rulesSearch);
        if ($valid->fails()) {
            return response()->json(array('message' => trans('response.invalid')), 400);
        }

        $resultBooks = $this->bookHandler->searchBooks($params['term']);

        if ($resultBooks->isEmpty())
        {
            return response()->json(array('message' => trans('response.no_search_result')), 404);
        }

        return response()->json($resultBooks, 200);
    }

    /**
     * Method for getting warehouse.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse|object
     */
    private function getBookById($id)
    {
        try
        {
            return $this->bookHandler->getBookById($id);
        } catch (\Exception $e) {
            Log::error('Error while getting record from DB: ', ['message' => $e->getMessage()]);
        }
    }
}

