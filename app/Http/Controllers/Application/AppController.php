<?php

namespace App\Http\Controllers\Application;

use App\Http\Controllers\Controller;

/**
 * Class AppController
 * @package App\Http\Controllers\Application
 */
class AppController extends Controller
{
    /**
     * Method for providing front layout.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function provideFrontend()
    {
        return view('layouts/frontend/master');
    }

    /**
     * Method for providing backend layout
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function provideBackend()
    {
        return view('layouts/backend/master');
    }


    /**
     * Method for choosing sub template.
     *
     * @param $viewName
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function choseSubTemplate($viewName)
    {
        switch ($viewName){
            case 'home.html':
                return view('frontend/home');
            case 'book.html':
                return view('frontend/book');
            case 'backend.html':
                return view('backend/login');
            case 'home-backend.html':
                return view('backend/home-backend');
            case 'signIn.html':
                return view('backend/login');
            case 'books.html':
                return view('backend/books');
            case 'book-edit.html':
                return view('backend/book-edit');
            case 'book-create.html':
                return view('backend/book-create');
        }
    }

}