<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Authentication
Route::post('/authenticate', 'Auth\AuthController@authenticate');
// Provide Template
Route::get('/', 'Application\AppController@provideFrontend');
Route::get('/backend', 'Application\AppController@provideBackend');
//Choose sub template
Route::get('/views/{viewName}', 'Application\AppController@choseSubTemplate');
//Search books
Route::post('/search', 'BookController@searchBooks');
//Book resource routes
Route::resource('book', 'BookController');
//Front home
Route::get('/home', 'BookController@index');

