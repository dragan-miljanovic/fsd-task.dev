<?php

/**
 * Class BookTest
 */
class BookTest extends TestCase
{
    /**
     * @var $token ;
     */
    private $token;

    /**
     * @var $book object
     */
    private $book;

    /**
     *Initial setup
     */
    public function setUp()
    {
        parent::setUp();

        $this->book = new \App\Models\Book\BookModel();
        $this->book->title = 'title test';
        $this->book->author = 'author test';
        $this->book->year_of_publication = '2001';
        $this->book->language = 'language test';
        $this->book->language_origin = 'language origin test';
        $this->book->save();

        $res   = $this->call('POST', '/authenticate', [ 'username' => 'admin@user.com', 'password' => 'admin', ], [ ],
            [ ], [ 'Content-Type' => 'application/json', 'HTTP_Authorization' => $this->token ], [ ]);
        $token = json_decode($res->getContent(), true);
        $this->token = "Bearer " . $token['token'];
        $this->refreshApplication();

    }

    /**
     * Test invalid methods.
     */
    public function testInvalidHttpMethods()
    {
        $response = $this->call('POST', '/book/' . 'invalid');
        $this->assertEquals(405, $response->status());

        $response = $this->call('PUT', '/book/');
        $this->assertEquals(405, $response->status());

        $response = $this->call('DELETE', '/book/');
        $this->assertEquals(405, $response->status());
    }

    /**
     * Test invalid authentication.
     */
    public function testInvalidAuthentication()
    {
        $response = $this->call('POST', '/book', [ ], [ ], [ ],
            [ 'CONTENT_TYPE' => 'application/json'], json_encode([
                'title'                 => 'title',
                'author'                => 'author',
                'year_of_publication'   => '2000',
                'language'              => 'language',
                'language_origin'       => 'language_origin',
            ]));
        $this->assertEquals(400, $response->status());
        $response = json_decode($response->getContent(), true);
        $this->assertEquals('token_not_provided', $response['error']);

        $response = $this->call('PUT', '/book/' . $this->book->id, [ ], [ ], [ ],
            [ 'CONTENT_TYPE' => 'application/json'], json_encode([
                'title'                     => 'title',
                'author'                    => 'author',
                'year_of_publication'       => '2000',
                'language'                  => 'language',
                'language_origin'           => 'language origin',
            ]));
        $this->assertEquals(400, $response->status());
        $response = json_decode($response->getContent(), true);
        $this->assertEquals('token_not_provided', $response['error']);

        $response = $this->call('DELETE', '/book/' . $this->book->id);
        $this->assertEquals(400, $response->status());
        $response = json_decode($response->getContent(), true);
        $this->assertEquals('token_not_provided', $response['error']);
    }

    /**
     * Test invalid http requests and params.
     */
    public function testInvalidCrud()
    {
        $response = $this->call('GET', '/book/' . 'invalid');
        $this->assertEquals(404, $response->status());
        $response = json_decode($response->getContent(), true);
        $this->assertEquals('Record not found.', $response['message']);

        $response = $this->call('POST', '/book', [ ], [ ], [ ],
            [ 'CONTENT_TYPE' => 'application/json', 'HTTP_Authorization' => $this->token ], json_encode([
                'title'                     => '',
                'author'                    => '',
                'year_of_publication'       => '',
                'language'                  => '',
                'language_origin'           => '',
            ]));
        $this->assertEquals(400, $response->status());
        $response = json_decode($response->getContent(), true);
        $this->assertEquals('Please send valid data.', $response['message']);

        $response = $this->call('POST', '/book', [ ], [ ], [ ],
            [ 'CONTENT_TYPE' => 'application/json', 'HTTP_Authorization' => $this->token ], json_encode([
                'title'                     => 'Titlelongerthan64characterTitlelongerthan64characterTitlelongerthan64character',
                'author'                    => 'Authorlongerthan64characterAuthorlongerthan64characterAuthorlongerthan64character',
                'year_of_publication'       => 'min',
                'language'                  => 'Languagelongerthan32characterLanguagelongerthan32character',
                'language_origin'           => 'Languageoriginlongerthan32character',
            ]));
        $this->assertEquals(400, $response->status());
        $response = json_decode($response->getContent(), true);
        $this->assertEquals('Please send valid data.', $response['message']);

        $response = $this->call('PUT', '/book/' . 'invalid', [ ], [ ], [ ],
            [ 'CONTENT_TYPE' => 'application/json', 'HTTP_Authorization' => $this->token ], json_encode([
                'title'                     => 'title',
                'author'                    => 'author',
                'year_of_publication'       => '2000',
                'language'                  => 'language',
                'language_origin'           => 'language origin',
            ]));;
        $this->assertEquals(404, $response->status());
        $response = json_decode($response->getContent(), true);
        $this->assertEquals('Record not found.', $response['message']);

        $response = $this->call('PUT', '/book/' . $this->book->id, [ ], [ ], [ ],
            [ 'CONTENT_TYPE' => 'application/json', 'HTTP_Authorization' => $this->token ], json_encode([
                'title'                     => '',
                'author'                    => '',
                'year_of_publication'       => '',
                'language'                  => '',
                'language_origin'           => '',
            ]));
        $this->assertEquals(400, $response->status());
        $response = json_decode($response->getContent(), true);
        $this->assertEquals('Please send valid data.', $response['message']);

        $response = $this->call('PUT', '/book/' . $this->book->id, [ ], [ ], [ ],
            [ 'CONTENT_TYPE' => 'application/json', 'HTTP_Authorization' => $this->token ], json_encode([
                'title'                     => 'Titlelongerthan64characterTitlelongerthan64characterTitlelongerthan64character',
                'author'                    => 'Authorlongerthan64characterAuthorlongerthan64characterAuthorlongerthan64character',
                'year_of_publication'       => 'min',
                'language'                  => 'Languagelongerthan32characterLanguagelongerthan32character',
                'language_origin'           => 'Languageoriginlongerthan32character',
            ]));
        $this->assertEquals(400, $response->status());
        $response = json_decode($response->getContent(), true);
        $this->assertEquals('Please send valid data.', $response['message']);

        $response = $this->call('DELETE', '/book/' . 'invalid', [ ], [ ], [ ],
            [ 'CONTENT_TYPE' => 'application/json', 'HTTP_Authorization' => $this->token ]);
        $this->assertEquals(404, $response->status());
        $response = json_decode($response->getContent(), true);
        $this->assertEquals('Record not found.', $response['message']);
    }

    /**
     * Method for testing valid http requests, params and authentication.
     */
    public function testValid()
    {
        $response = $this->call('GET', '/book', [ ], [ ], [ ],
            [ 'CONTENT_TYPE' => 'application/json' ]);
        $this->assertEquals(200, $response->status());
        $this->seeJsonStructure([
            'message',
            'books' => [
                ['title', 'author', 'year_of_publication', 'language', 'language_origin']
            ]
        ]);
        $response = json_decode($response->getContent(), true);
        $this->assertEquals('Success.', $response['message']);

        $response = $this->call('GET', '/book/' . $this->book->id, [ ], [ ], [ ],
            [ 'CONTENT_TYPE' => 'application/json' ]);
        $this->assertEquals(200, $response->status());
        $this->seeJsonStructure([
            'message',
            'book' => [
                'title', 'author', 'year_of_publication', 'language', 'language_origin'
            ]
        ]);
        $response = json_decode($response->getContent(), true);
        $this->assertEquals('Success.', $response['message']);

        $response = $this->call('POST', '/book', [ ], [ ], [ ],
            [ 'CONTENT_TYPE' => 'application/json', 'HTTP_Authorization' => $this->token ], json_encode([
                'title'                 => 'title',
                'author'                => 'author',
                'year_of_publication'   => '2000',
                'language'              => 'language',
                'language_origin'       => 'language_origin',
            ]));
        $this->assertEquals(200, $response->status());
        $response = json_decode($response->getContent(), true);
        $this->assertEquals('Record successfully created.', $response['message']);

        $response = $this->call('PUT', '/book/' . $this->book->id, [ ], [ ], [ ],
            [ 'CONTENT_TYPE' => 'application/json', 'HTTP_Authorization' => $this->token ], json_encode([
                'title'                     => 'title',
                'author'                    => 'author',
                'year_of_publication'       => '2000',
                'language'                  => 'language',
                'language_origin'           => 'language origin',
            ]));
        $this->assertEquals(200, $response->status());
        $response = json_decode($response->getContent(), true);
        $this->assertEquals('Record successfully updated.', $response['message']);

        $response = $this->call('DELETE', '/book/' . $this->book->id, [ ], [ ], [ ],
            [ 'CONTENT_TYPE' => 'application/json', 'HTTP_Authorization' => $this->token ]);
        $this->assertEquals(200, $response->status());
        $response = json_decode($response->getContent(), true);
        $this->assertEquals('Record successfully deleted.', $response['message']);
    }

    /**
     *  Deletes test data.
     */
    public function tearDown()
    {
        DB::delete("DELETE FROM books WHERE id = LAST_INSERT_ID()");
    }
}