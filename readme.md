# Install

* Clone repo

* From your console at your root folder execute 'composer install' to install dependencies.

* At your project root, rename '.env.example' file to '.env'

    * Set DB_DATABASE=fsd DB_USERNAME=root DB_PASSWORD=root

    * From your console at your root folder execute 'php artisan key:generate' to generate and set APP_KEY

* From your console at your root folder execute 'sh checkall.sh' to start migrations and seeders

* To enter front of the app fsd-task.dev

* To enter backend of the app fsd-task.dev/backend

    * user: admin@user.com

    * pass: admin