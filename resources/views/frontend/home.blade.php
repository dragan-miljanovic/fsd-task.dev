<div class="container" ng-controller="HomeCtrl">
    <form class="row col-lg-4 col-lg-offset-8">
        <div class="form-group">
            <label>Search</label>
            <input type="text" ng-model="search" class="form-control" placeholder="Search">
        </div>
    </form>

    <div class="row">

        <div class="row col-lg-11 col-lg-offset-1">
            <dir-pagination-controls
                max-size="5"
                direction-links="true"
                boundary-links="true"
                auto-hide="false"
                on-page-change="pageChanged()">
            </dir-pagination-controls>
        </div>

        <div class="col-lg-11 col-lg-offset-1" ng-init="getAllBooks()">
            <span class="text-danger" ng-if="(books|filter:search).length < 1">Sorry, there is no search result.</span>
            <span dir-paginate="book in books|filter:search|itemsPerPage:9">
            <div class="col-md-3 col-sm-8  hero-feature" style="width: 33%">
                <div class="thumbnail">
                    <div class="caption">
                        <img class="img-responsive bookImg"
                             ng-src="http://www.bastidirectory.com/images/post/1415297277-Basti-Directory-6741.png" alt="">
                    <div class="caption">
                        <h3>[[book.title]]</h3>
                        <p>Author: [[book.author]]</p>
                        <p>Year of pub.: [[book.year_of_publication]]</p>
                        <p>Language: [[book.language]]</p>
                        <p>Lang. origin: [[book.language_origin]]</p>
                        <p>
                            <a href="#/book/[[book.id]]" class="btn btn-info">More info</a>
                        </p>
                    </div>
                    </div>
                </div>
            </div>
            </span>
        </div>

        <div class="row col-lg-11 col-lg-offset-1">
            <dir-pagination-controls
                max-size="5"
                direction-links="true"
                boundary-links="true"
                auto-hide="false"
                on-page-change="pageChanged()">
            </dir-pagination-controls>
        </div>
    </div>
</div>
