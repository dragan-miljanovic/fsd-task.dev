<div role="main" class="container theme-showcase" ng-controller="BookCtrl">
    <div class="col-lg-8 col-lg-offset-1">
        <div class="page-header">
            <h3>
                <span class="semi-bold">
                    [[selected_book.title]]
                </span>
            </h3>
        </div>
        <div class="bs-component">
            <div ng-init="getBook()">

                <div id="bookImgHolder">
                    <img
                        src="http://www.bastidirectory.com/images/post/1415297277-Basti-Directory-6741.png">
                </div>

                <div id="dataHolder">
                    <table class="table table-striped table-hover">
                        <tbody>
                        <tr>
                            <th>Author</th>
                            <td>[[selected_book.author]]</td>
                        </tr>
                        <tr>
                            <th>Year of publication</th>
                            <td>[[selected_book.year_of_publication]]</td>
                        </tr>
                        <tr>
                            <th>Language</th>
                            <td>[[selected_book.language]]</td>
                        </tr>
                        <tr>
                            <th>Language origin</th>
                            <td>[[selected_book.language_origin]]</td>
                        </tr>
                        </tbody>
                    </table>

                    <a ng-href="#/home">
                        <button class="btn btn-success">Back</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
