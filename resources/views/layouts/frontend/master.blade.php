<!DOCTYPE html>
<html ng-app="uiApp">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>FSD task</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="{{ URL::asset('app/bower_components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body ng-cloak>

<!--nav-->
<ng-include src="views/header"></ng-include>

<nav>
    <div>
        <div id="main-menu">
            <div ui-view></div>
        </div>
    </div>
</nav>

<!-- build:js scripts/vendor.js -->
<!-- bower:js -->
<script src="{{ URL::asset('app/bower_components/angular/angular.js') }}"></script>
<script src="{{ URL::asset('app/bower_components/angular-resource/angular-resource.js') }}"></script>
<script src="{{ URL::asset('app/bower_components/angular-messages/angular-messages.js') }}"></script>
<script src="{{ URL::asset('app/bower_components/angular-animate/angular-animate.js') }}"></script>
<script src="{{ URL::asset('app/bower_components/angular-strap/dist/angular-strap.js') }}"></script>
<script src="{{ URL::asset('app/bower_components/angular-strap/dist/angular-strap.tpl.js') }}"></script>
<script src="{{ URL::asset('app/bower_components/angular-route/angular-route.js') }}"></script>
<script src="{{ URL::asset('app/bower_components/angular-cookies/angular-cookies.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-router/0.4.2/angular-ui-router.min.js"></script>
<script src="{{ URL::asset('app/bower_components/angularUtils-pagination/dirPagination.js') }}"></script>


<script src="{{ URL::asset('app/lib/satellizer.js') }}"></script>

<script src="{{ URL::asset('app/scripts/frontend.js') }}"></script>
<script src="{{ URL::asset('app/scripts/controllers/HomeCtrl.js') }}"></script>
<script src="{{ URL::asset('app/scripts/services/HomeFactory.js') }}"></script>
<script src="{{ URL::asset('app/scripts/controllers/BookCtrl.js') }}"></script>
<script src="{{ URL::asset('app/scripts/services/BookFactory.js') }}"></script>

</body>
</html>