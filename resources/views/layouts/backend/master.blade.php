<!DOCTYPE html>
<html ng-app="uiAdmin">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>FSD task - admin</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="{{ URL::asset('app/bower_components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('css/main.css') }}" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->

</head>
<body
<body class="" ng-controller="AdminCtrl">

<div ng-if="isAuthenticated()" ng-init="init()">
    <header class="navbar navbar-inverse navbar-fixed-top wet-asphalt" role="banner">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="">
                    <img src="https://www.fsd.rs/assets/img/fsd-logo.svg" alt="logo"></a>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#/books">{{trans('admin.all_books') }}</a></li>
                    <li>
                        <a href="#/book"> {{ trans('admin.create_book') }}</a>
                    </li>
                    <li><a href="#/signOut">{{ trans('admin.signout') }}</a></li>
                </ul>
            </div>
        </div>
    </header><!--/header-->
</div>
<!-- BEGIN PAGE CONTAINER-->
<div class="page-content" id="page-content">
    <div class="content sm-gutter">
        <div ui-view></div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>-->

<!-- END CONTAINER -->
<!--[if lt IE 9]>
<script src="bower_components/es5-shim/es5-shim.js"></script>
<script src="bower_components/json3/lib/json3.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/es5-shim/2.2.0/es5-shim.js"></script>
<script>
    document.createElement('ui-select');
    document.createElement('ui-select-match');
    document.createElement('ui-select-choices');
</script>
<![endif]-->


<!-- bower:js -->
<script src="{{ URL::asset('app/bower_components/angular/angular.js') }}"></script>
<script src="{{ URL::asset('app/bower_components/angular-resource/angular-resource.js') }}"></script>
<script src="{{ URL::asset('app/bower_components/angular-messages/angular-messages.js') }}"></script>
<script src="{{ URL::asset('app/bower_components/angular-animate/angular-animate.js') }}"></script>
<script src="{{ URL::asset('app/bower_components/angular-strap/dist/angular-strap.js') }}"></script>
<script src="{{ URL::asset('app/bower_components/angular-strap/dist/angular-strap.tpl.js') }}"></script>
<script src="{{ URL::asset('app/bower_components/angular-route/angular-route.js') }}"></script>
<script src="{{ URL::asset('app/bower_components/angular-cookies/angular-cookies.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-router/0.4.2/angular-ui-router.min.js"></script>
<script src="{{ URL::asset('app/bower_components/angular-ui-select/dist/select.js') }}"></script>
<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.7/angular-sanitize.js"></script>
<script src="{{ URL::asset('app/bower_components/angularUtils-pagination/dirPagination.js') }}"></script>

<script src="{{ URL::asset('app/lib/satellizer.js') }}"></script>

<script src="{{ URL::asset('app/scripts/backend.js') }}"></script>
<script src="{{ URL::asset('app/scripts/controllers/AdminCtrl.js') }}"></script>
<script src="{{ URL::asset('app/scripts/controllers/SignOutCtrl.js') }}"></script>
<script src="{{ URL::asset('app/scripts/controllers/BookCtrl.js') }}"></script>
<script src="{{ URL::asset('app/scripts/services/BookFactory.js') }}"></script>

</body>
</html>