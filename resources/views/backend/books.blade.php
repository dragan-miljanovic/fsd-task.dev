<div role="main" class="container theme-showcase" ng-controller="BookCtrl">
    <div class="col-lg-12">
        <div class="page-header">
            <h3>Books</h3>
        </div>
        <div class="bs-component" ng-if="isAuthenticated()">
            <form class="form-inline">
                <div class="form-group">
                    <label>Search</label>
                    <input type="text" ng-model="search" class="form-control" placeholder="Search">
                </div>
            </form>
            <div ng-init="getAllBooks()">
                <table class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th ng-click="sort('title')">Title
                            <span class="glyphicon sort-icon" ng-show="sortKey=='title'"
                                  ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                        </th>
                        <th ng-click="sort('author')">Author
                            <span class="glyphicon sort-icon" ng-show="sortKey=='author'"
                                  ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                        </th>
                        <th ng-click="sort('year_of_publication')">Year Of Publication
                            <span class="glyphicon sort-icon" ng-show="sortKey=='year_of_publication'"
                                  ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                        </th>
                        <th ng-click="sort('language')">Language
                            <span class="glyphicon sort-icon" ng-show="sortKey=='language'"
                                  ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                        </th>
                        <th ng-click="sort('language_origin')">Language origin
                            <span class="glyphicon sort-icon" ng-show="sortKey=='language_origin'"
                                  ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                        </th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    </thead>
                    <tbody>
                    <tr dir-paginate="book in books|filter:search|orderBy:sortKey:reverse|itemsPerPage:10">
                        <td>[[book.title]]</td>
                        <td>[[book.author]]</td>
                        <td>[[book.year_of_publication]]</td>
                        <td>[[book.language]]</td>
                        <td>[[book.language_origin]]</td>
                        <td>
                            <a ng-href="#/book/[[book.id]]/edit">
                                <button class="btn btn-primary">Edit</button>
                            </a>
                            <a ng-click="deleteBook(book.id)">
                                <button class="btn btn-danger">Delete</button>
                            </a>
                        </td>
                    </tr>
                    </tbody>
                    <dir-pagination-controls
                        max-size="5"
                        direction-links="true"
                        boundary-links="true">
                    </dir-pagination-controls>
                </table>
            </div>
        </div>
    </div>
</div>