<div ng-controller="BookCtrl" style="margin-top: 10%;">
    <div class="container">
        <div class="row">
            <h4>New Book</h4>
        </div>
        <div class="row">
            <form name="form" id='form' autocomplete="off"
                  ng-submit="createBook()" novalidate class="form-horizontal">
                <div class="form-group">
                    <label for="title_eng" class="col-sm-2 control-label">Title</label>
                    <div class="col-sm-6">
                        <input id="title"
                               name="title"
                               type="text"
                               placeholder="Title"
                               class="form-control"
                               required
                               ng-model="book_create.title"
                               ng-minlength="4"
                               ng-maxlength="64">
                        <span class="text-danger"
                              ng-show="(form.title.$dirty || form.$submitted) && form.title.$error.required">
                            {{ trans('book.required') }}
                        </span>
                        <span class="text-danger"
                              ng-show="(form.title.$dirty || form.$submitted) && form.title.$error.minlength">
                            {{ trans('book.title_minlength') }}
                        </span>
                        <span class="text-danger"
                              ng-show="(form.title.$dirty || form.$submitted) && form.title.$error.maxlength">
                            {{ trans('book.title_maxlength') }}
                        </span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="author" class="col-sm-2 control-label">Author</label>
                    <div class="controls col-sm-6">
                        <div class="controls">
                            <input id="author"
                                   name="author"
                                   type="text"
                                   placeholder="Author"
                                   class="form-control"
                                   required
                                   ng-model="book_create.author"
                                   ng-minlength="4"
                                   ng-maxlength="64">
                            <span class="text-danger"
                                  ng-show="(form.auhtor.$dirty || form.$submitted) && form.auhtor.$error.required">
                                {{ trans('book.required') }}
                            </span>
                            <span class="text-danger"
                                  ng-show="(form.auhtor.$dirty || form.$submitted) && form.auhtor.$error.minlength">
                                {{ trans('book.auhtor_minlength') }}
                            </span>
                            <span class="text-danger"
                                  ng-show="(form.auhtor.$dirty || form.$submitted) && form.auhtor.$error.maxlength">
                                {{ trans('book.auhtor_maxlength') }}
                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="year_of_publication" class="col-sm-2 control-label">Year Of Publication</label>
                    <div class="col-sm-6">
                        <input id="year_of_publication"
                               name="year_of_publication"
                               type="text"
                               placeholder="Year of publication"
                               class="form-control"
                               required
                               ng-model="book_create.year_of_publication"
                               ng-maxlength="4">
                        <span class="text-danger"
                              ng-show="(form.year_of_publication.$dirty || form.$submitted) && form.year_of_publication.$error.required">
                            {{ trans('book.required') }}
                        </span>
                        <span class="text-danger"
                              ng-show="(form.year_of_publication.$dirty || form.$submitted) && form.year_of_publication.$error.maxlength">
                            {{ trans('book.year_of_publication_maxlength') }}
                        </span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="language" class="col-sm-2 control-label">Language</label>
                    <div class="controls col-sm-6">
                        <input id="language"
                               name="language"
                               type="text"
                               placeholder="Language"
                               class="form-control"
                               required
                               ng-model="book_create.language"
                               ng-minlength="4"
                               ng-maxlength="32">
                        <span class="text-danger"
                              ng-show="(form.language.$dirty || form.$submitted) && form.language.$error.required">
                            {{ trans('book.required') }}
                        </span>
                        <span class="text-danger"
                              ng-show="(form.language.$dirty || form.$submitted) && form.language.$error.minlength">
                            {{ trans('book.language_minlength') }}
                        </span>
                        <span class="text-danger"
                              ng-show="(form.language.$dirty || form.$submitted) && form.language.$error.maxlength">
                            {{ trans('book.language_maxlength') }}
                        </span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="language_origin" class="col-sm-2 control-label">Language origin</label>
                    <div class="controls col-sm-6">
                        <input id="language_origin"
                               name="language_origin"
                               type="text"
                               placeholder="Language origin"
                               class="form-control"
                               required
                               ng-model="book_create.language_origin"
                               ng-minlength="4"
                               ng-maxlength="32">
                        <span class="text-danger"
                              ng-show="(form.language_origin.$dirty || form.$submitted) && form.language_origin.$error.required">
                            {{ trans('book.required') }}
                        </span>
                        <span class="text-danger"
                              ng-show="(form.language_origin.$dirty || form.$submitted) && form.language_origin.$error.minlength">
                            {{ trans('book.language_origin_minlength') }}
                        </span>
                        <span class="text-danger"
                              ng-show="(form.language_origin.$dirty || form.$submitted) && form.language_origin.$error.maxlength">
                            {{ trans('book.language_origin_maxlength') }}
                        </span>
                    </div>
                </div>

                <div class="row col-sm-offset-2">
                    <button ng-disabled="form.$invalid" type="submit" class="btn btn-primary" value="book">Create
                    </button>
                    <a ng-href="#/books">
                        <button type="button" class="btn btn-success">Back</button>
                    </a>
                </div>
            </form>
        </div>
    </div>

</div>

<!-- Modal -->
<div class="modal fade" id="upload" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">{{ trans('company.upload') }}</h4>
            </div>
            <div class="modal-body">
                <div id="dropzone" class="dropzone" ng-init="dropzone('/upload')" ng-controller="BookCtrl">
                    <div class="fallback">
                        <input name="file" type="file" multiple/>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('buttons.close') }}</button>
            </div>
        </div>
    </div>
</div>

