<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'no_data'               => 'No records found.',
    'invalid'               => 'Please send valid data.',
    'not_created'           => 'Record not created. Please try again.',
    'created'               => 'Record successfully created.',
    'not_found'             => 'Record not found.',
    'not_updated'           => 'Record not updated. Please try again.',
    'updated'               => 'Record successfully updated.',
    'not_deleted'           => 'Record not deleted. Please try again',
    'deleted'               => 'Record successfully deleted.',
    'success'               => 'Success.',
    'not_fetched'           => 'Sorry, data could not be fetched.',
    'no_search_result'      => 'No search result for requested term.'


];