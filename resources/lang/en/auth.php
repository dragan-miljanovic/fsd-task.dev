<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'empty'                     => 'Please send some date.',
    'invalid'                   => 'Please send valid data.',
    'user_not_found'            => 'User not found',
    'user_not_active'           => 'User not active',
    'could_not_create_token'    => 'Token could not be created. Please try again.',

];
