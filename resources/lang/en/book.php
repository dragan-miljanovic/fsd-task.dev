<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Response Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during response for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'title'                             => 'Tile',
    'author'                            => 'Autor',
    'year_of_publication'               => 'Year of publication',
    'language'                          => 'Language',
    'language_origin'                   => 'Language origin',
    'required'                          => 'Required',
    'title_maxlength'                   => 'Title max length is 64 characters',
    'title_minlength'                   => 'Title min length is 4 characters',
    'auhtor_minlength'                  => 'Author min length is 4 characters',
    'auhtor_maxlength'                  => 'Author max length is 64 characters',
    'language_minlength'                => 'Language min length is 4 characters',
    'language_maxlength'                => 'Language max length is 32 characters',
    'language_origin_minlength'         => 'Language origin min length is 4 characters',
    'language_origin_maxlength'         => 'Language origin max length is 32 characters',
    'year_of_publication_maxlength'     => 'Year of publication max length is 4 characters',
];