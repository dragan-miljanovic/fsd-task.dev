<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Response Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during response for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'signout'                   => 'Sign out',
    'books'                     => 'Books',
    'create_book'               => 'Create book',
    'all_books'                 => 'All Books',
    'required'                  => 'Required',
    'invalid_email'             => 'Invalid email address',
];